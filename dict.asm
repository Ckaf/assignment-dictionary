global find_word
extern exit
extern string_equals
section .text

;rdi, rsi, rdx, rcx, r8, r9

; rdi - Указатель на нуль-терминированную строку.
; rsi - Указатель на начало словаря.

;find_word пройдёт по всему словарю в поисках подходящего ключа.
;Если подходящее вхождение найдено, вернёт адрес начала вхождения в словарь (не значения), иначе вернёт 0.

find_word:
    push rsi
    .loop:
    add rsi, 8
    call string_equals
    sub rsi, 8
    cmp rax, 1
    je .equal
    mov rsi, [rsi]
    cmp rsi, 0
    je .return_0
    jmp .loop
    .equal:
    mov rax, rsi
    pop rsi
    ret
    .return_0:
    mov rax, 0
    pop rsi
    ret