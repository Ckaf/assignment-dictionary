;Макрос для создания слов в словаре.

;Макрос принимает два параметра:
;Ключ (в кавычках)
;Имя метки, по которой будет находиться значение.

%define next_address 0

%macro colon 2
 %2:
 dq next_address
 db %1, 0
 %define next_address %2
%endmacro
