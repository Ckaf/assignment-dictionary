;Читает строку размером не более 255 символов в буфер с stdin.
;Пытается найти вхождение в словаре; если оно найдено, распечатывает в stdout значение по этому ключу. Иначе выдает сообщение об ошибке.
%include "lib.inc"
extern find_word
global _start

section .data
    %include "words.inc"
    buffer: times 255 db 0
    inp_error: db 'Data entry error', 0
    key_error: db 'Invalid key', 0
section .text
%define rbytes 8
%define stdout 1
%define stderr 2
%define limit 255
print_error:
    push rdx
    push rsi
    push rax
    push rdi
    call string_length
    mov rsi, rdi
    mov rdx, rax
    mov rax, stdout
    mov rdi, stderr
    syscall
    pop rdi
    pop rax
    pop rsi
    pop rdx
    ret

_start:
    mov rdi, buffer
    mov rsi, limit
    call read_word
    cmp rax, 0
    je .error_input
    mov rsi, next_address
    call find_word
    cmp rax, 0
    je .error_key

    mov rdi, rax
    add rdi, rbytes
    call string_length
    add rdi, rax
    inc rdi
    call print_string
    call print_newline
    call exit
    .error_input:
        mov rdi, inp_error
        call print_error
        call print_newline
        call exit

    .error_key:
        mov rdi, key_error
        call print_error
        call print_newline
        call exit