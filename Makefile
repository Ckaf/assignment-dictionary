COMPILE = nasm -f elf64 -o
DIR = lab2
COLLECTOR = ld -o
lib.o: lib.asm
	 $(COMPILE) lib.o lib.asm
lib_inc.o: lib.inc lib.o
	$(COMPILE) lib_inc.o lib.inc
dict.o: dict.asm lib.o
	$(COMPILE) dict.o dict.asm
main.o: main.asm dict.o words.inc lib_inc.o
	$(COMPILE) main.o main.asm
lab2: main.o dict.o lib.o lib_inc.o
	$(COLLECTOR) $(DIR) lib.o lib_inc.o dict.o main.o

clean:
	rm -rf *.o $(DIR)

.PHONY: clean